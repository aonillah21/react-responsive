/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import './HomePage.css'

export default function HomePage() {
  return (
    <Container>
      <Row>
        <Col sm>sm</Col>
        <Col sm>sm</Col>
        <Col sm>sm</Col>
      </Row>
      <Row>
        <Col md>md</Col>
        <Col md>md</Col>
        <Col md>md</Col>
      </Row>
      <Row>
        <Col xl>xl</Col>
        <Col xl>xl</Col>
        <Col xl>xl</Col>
      </Row>

      {/* Stack the columns on mobile by making one full-width and the other half-width */}
      <Row>
        <Col xs={12} md={8}>
          xs=12 md=8
        </Col>
        <Col xs={6} md={4}>
          xs=6 md=4
        </Col>
      </Row>

      {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}
      <Row>
        <Col xs={6} md={4}>
          xs=6 md=4
        </Col>
        <Col xs={6} md={4}>
          xs=6 md=4
        </Col>
        <Col xs={6} md={4}>
          xs=6 md=4
        </Col>
      </Row>

      {/* Columns are always 50% wide, on mobile and desktop */}
      <Row>
        <Col xs={6}>xs=6</Col>
        <Col xs={6}>xs=6</Col>
      </Row>
    </Container>
  );
}
